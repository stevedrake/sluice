import os
import os.path
import tempfile
import unittest

from weir import process, zfs


tempzfs = None

def gettempzfs():
	# XXX: not thread-safe
	global tempzfs
	if tempzfs is None:
		# assemble list of possible tmp filesystems
		tmps = []

		# try fs specified in environment first
		if 'TMPZFS' in os.environ:
			tmps.append(os.environ['TMPZFS'])

		# also try tmp fs for common pool names
		tmps.extend(['rpool/tmp', 'zroot/tmp'])

		# check if any of the candidates is acceptable
		for tmp in tmps:
			try:
				fs = mkzfstemp(parent=tmp, props={'canmount': 'noauto'})
				fs.destroy()

				tempzfs = tmp
				break
			except (OSError, IOError, process.CalledProcessError):
				pass
		else:
			# no usable temporary zfs filesystem found in tmps
			raise process.DatasetNotFoundError(tmps)

	return tempzfs

def mkzfstemp(suffix='', prefix='tmp', parent=None, props={}):
	if parent is None:
		parent = gettempzfs()

	# don't allow setting the mountpoint, since generating
	# it is how we currently get the filesystem name
	if 'mountpoint' in props:
		raise NotImplementedError('setting the mountpoint is not supported')

	dir = zfs.open(parent).getpropval('mountpoint')
	mnt = tempfile.mkdtemp(suffix=suffix, prefix=prefix, dir=dir)
	try:
		name = parent + '/' + os.path.basename(mnt)
		return zfs.create(name, props=props)
	except:
		os.rmdir(mnt)
		raise

class SluiceTestCase(unittest.TestCase):
	prefix = 'sluice-'

	def setUp(self):
		# create temporary filesystem for this test
		self.filesystem = mkzfstemp(
			prefix=self.prefix, props={'canmount': 'noauto'})

	def tearDown(self):
		# destroy temporary filesystem
		self.filesystem.destroy(force=True)
