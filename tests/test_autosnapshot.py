import unittest
from datetime import datetime

from weir import process, zfs

from sluice.autosnapshot import autosnapshot

from tests import support


# integration tests for autosnapshot
class AutosnapshotTest(support.SluiceTestCase):
	prefix = 'sluice.autosnapshot-'

	# check that snapshot is created with name according to specified format
	def test_autosnapshot_with_explicit_format(self):
		# use a coarse timestamp format to avoid changes during test
		snap_format = '%Y-%j'
		# create snapshot with explicit timestamp format
		autosnapshot(self.filesystem, snap_format)

		# check snapshot was created with name according to specified format
		snap_name = datetime.now().strftime(snap_format)
		try:
			zfs.open(self.filesystem.name + '@' + snap_name)
		except process.DatasetNotFoundError:
			self.fail('expected snapshot was not found')

	# check that snapshot is created with name according to format property
	def test_autosnapshot_with_format_property(self):
		# use a coarse timestamp format to avoid changes during test
		snap_format = '%Y-%j'
		# set timestamp format property
		self.filesystem.setprop('sluice.autosnapshot:format', snap_format)
		# create snapshot without specifying timestamp format
		autosnapshot(self.filesystem)

		# check snapshot was created with expected name
		snap_name = datetime.now().strftime(snap_format)
		try:
			zfs.open(self.filesystem.name + '@' + snap_name)
		except process.DatasetNotFoundError:
			self.fail('expected snapshot was not found')

if __name__ == '__main__':
	unittest.main()
